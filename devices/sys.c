/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-SYS - System controller virtual device
 */
#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>

#include <devices/sys.h>
#define DBG_ITEM DBG_SYS
#include <lib/debug.h>
#include <platform/iomem.h>

/*
 * Frontend: LupIO-SYS virtual device implementation
 */
enum {
    LUPIO_SYS_HALT          = 0x0,
    LUPIO_SYS_REBT          = 0x4,

    /* Top offset in register map */
    LUPIO_SYS_OFFSET_MAX    = 0x8,
};

static uint64_t lupio_sys_read(void *dev, phys_addr_t offset,
                               enum iomem_size_log2 size_log2)
{
    dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
             __func__, offset);

    return 0;
}

static void lupio_sys_write(void *dev, phys_addr_t offset, uint64_t val, enum
                            iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_SYS_HALT:
            exit(val);

        case LUPIO_SYS_REBT:
            /* Unimplemented */
        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_sys_ops = {
    .read = lupio_sys_read,
    .write = lupio_sys_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_sys_init(phys_addr_t base, phys_addr_t size)
{
    assert(size >= LUPIO_SYS_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_sys_ops);
}
