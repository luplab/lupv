/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2021 Joël Porquet-Lupine
 *
 * LupIO-RTC - Realtime clock virtual device
 */
#ifndef RTC_H
#define RTC_H

#include <stdint.h>

#include <cpu/cpu.h>

/*
 * LupIO-RTC device interface
 */
void lupio_rtc_init(phys_addr_t base, phys_addr_t size);

#endif /* RTC_H */
