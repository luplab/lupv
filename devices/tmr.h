/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-TMR - Timer virtual device
 */
#ifndef TMR_H
#define TMR_H

#include <stdint.h>

#include <cpu/cpu.h>

#define TIMEBASE_FREQ 10000000      /* 10 MHz frequency */

/*
 * LupIO-TMR device interface
 */
void lupio_tmr_init(phys_addr_t base, phys_addr_t size, int cpu_irq);
int lupio_tmr_expiration(void);

#endif /* TMR_H */
