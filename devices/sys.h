/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-SYS - System controller virtual device
 */
#ifndef SYS_H
#define SYS_H

#include <stdint.h>

#include <cpu/cpu.h>

/*
 * LupIO-SYS device interface
 */
void lupio_sys_init(phys_addr_t base, phys_addr_t size);

#endif /* SYS_H */
