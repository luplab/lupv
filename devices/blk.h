/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2003-2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Lupio-BLK - Block virtual device
 */
#ifndef BLK_H
#define BLK_H

#include <stdint.h>

#include <cpu/cpu.h>

/**
 * enum disk_img_mode - Disk image mode
 * @DISK_IMG_PV: Disk content can be modified, but modifications are private to
 *               emulator and are not written back to disk image. This is the
 *               default mode.
 * @DISK_IMG_RW: Disk content can be modified, and modifications are written
 *               back to disk image.
 * @DISK_IMG_RO: Disk content cannot be modified.
 */
enum disk_img_mode {
    DISK_IMG_PV = 0,
    DISK_IMG_RW,
    DISK_IMG_RO,
};

void disk_img_init(const char *filename, enum disk_img_mode mode);
void disk_img_exit(void);

/*
 * LupIO-BLK device interface
 */
void lupio_blk_init(phys_addr_t base, phys_addr_t size, int irq_num);

#endif /* BLK_H */
