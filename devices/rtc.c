/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2021 Joël Porquet-Lupine
 *
 * LupIO-RTC - Realtime clock virtual device
 */

#include <assert.h>
#include <inttypes.h>
#include <time.h>

#include <devices/rtc.h>
#define DBG_ITEM DBG_RTC
#include <lib/debug.h>
#include <platform/iomem.h>

/*
 * Frontend: LupIO-RTC virtual device implementation
 */
enum {
    LUPIO_RTC_SECD  = 0x0,
    LUPIO_RTC_MINT,
    LUPIO_RTC_HOUR,
    LUPIO_RTC_DYMO,
    LUPIO_RTC_MNTH,
    LUPIO_RTC_YEAR,
    LUPIO_RTC_CENT,
    LUPIO_RTC_DYWK,
    LUPIO_RTC_DYYR,

    /* Top offset in register map */
    LUPIO_RTC_OFFSET_MAX,
};

static uint64_t lupio_rtc_read(void *dev, phys_addr_t offset,
                               enum iomem_size_log2 size_log2)
{
    uint32_t val = 0;
    time_t time_sec;
    struct tm time_bd;
    char buffer[256];

    /* Get real time  in seconds */
    time_sec = time(NULL);
    /* Break down time representation */
    gmtime_r(&time_sec, &time_bd);

    strftime(buffer, 256, "%c", &time_bd);
    dbg_more("%s: read time = %s", __func__, buffer);

    switch(offset) {
        case LUPIO_RTC_SECD:
            val = time_bd.tm_sec;                 /* 0-60 (for leap seconds) */
            break;
        case LUPIO_RTC_MINT:
            val = time_bd.tm_min;                 /* 0-59 */
            break;
        case LUPIO_RTC_HOUR:
            val = time_bd.tm_hour;                /* 0-23 */
            break;
        case LUPIO_RTC_DYMO:
            val = time_bd.tm_mday;                /* 1-31 */
            break;
        case LUPIO_RTC_MNTH:
            val = time_bd.tm_mon + 1;             /* 1-12 */
            break;
        case LUPIO_RTC_YEAR:
            val = (time_bd.tm_year + 1900) % 100; /* 0-99 */
            break;
        case LUPIO_RTC_CENT:
            val = (time_bd.tm_year + 1900) / 100; /* 0-99 */
            break;
        case LUPIO_RTC_DYWK:
            val = 1 + (time_bd.tm_wday + 6) % 7;  /* 1-7 (Monday is 1) */
            break;
        case LUPIO_RTC_DYYR:
            val = time_bd.tm_yday + 1;            /* 1-366 (for leap years) */
            break;

        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }

    return val;
}

static void lupio_rtc_write(void *dev, phys_addr_t offset, uint64_t val,
                            enum iomem_size_log2 size_log2)
{
    dbg_core("%s: invalid write access at offset=0x%" PRIxpa, __func__, offset);
}

static struct iomem_dev_ops lupio_rtc_ops = {
    .read = lupio_rtc_read,
    .write = lupio_rtc_write,
    .flags = IOMEM_DEV_SIZE8,
};

void lupio_rtc_init(phys_addr_t base, phys_addr_t size)
{
    assert(size >= LUPIO_RTC_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_rtc_ops);
}
