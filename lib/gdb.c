/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 * Copyright (c) 2020 Joël Porquet-Lupine
 *
 * Support for debugging with GDB via its remote serial protocol
 */

#include <assert.h>
#include <netinet/in.h>
#include <poll.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cpu/cpu_private.h>
#include <lib/byteorder.h>
#define DBG_ITEM DBG_GDB
#include <lib/debug.h>
#include <lib/gdb.h>
#include <lib/utils.h>

/*
 * Code sent by GDB client to interrupt running program in between ordinary
 * packets
 */
#define INTERRUPT_CODE 0x3

/*
 * Size of buffer used to store packets sent from GDB client
 * This value is communicated to the client
 */
#define PACKET_BUFFER_SIZE 1024

/*
 * Maximum number of breakpoints that can be set at a time
 */
#define MAX_BREAKPOINTS 128
static cpu_uint_t breakpoints[MAX_BREAKPOINTS];
static size_t num_breakpoints = 0;


static int socket_desc = -1, connected_socket = -1;
static bool server_enabled = false;
static bool single_step_flag = false;

/* XML description of the CPU required by GDB client */
static const char cpu_desc_xml[] =
    "<?xml version=\"1.0\"?>"
    "<!DOCTYPE target SYSTEM \"gdb-target.dtd\">"
    "<target version=\"1.0\">"
    "<architecture>riscv:rv" str(CPU_XLEN) "</architecture>"
    "<feature name=\"org.gnu.gdb.riscv.cpu\">"
    "<reg name=\"zero\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\" regnum=\"0\"/>"
    "<reg name=\"ra\" bitsize=\"" str(CPU_XLEN) "\" type=\"code_ptr\"/>"
    "<reg name=\"sp\" bitsize=\"" str(CPU_XLEN) "\" type=\"data_ptr\"/>"
    "<reg name=\"gp\" bitsize=\"" str(CPU_XLEN) "\" type=\"data_ptr\"/>"
    "<reg name=\"tp\" bitsize=\"" str(CPU_XLEN) "\" type=\"data_ptr\"/>"
    "<reg name=\"t0\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t1\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t2\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"fp\" bitsize=\"" str(CPU_XLEN) "\" type=\"data_ptr\"/>"
    "<reg name=\"s1\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a0\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a1\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a2\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a3\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a4\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a5\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a6\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"a7\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s2\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s3\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s4\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s5\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s6\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s7\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s8\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s9\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s10\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"s11\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t3\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t4\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t5\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"t6\" bitsize=\"" str(CPU_XLEN) "\" type=\"int\"/>"
    "<reg name=\"pc\" bitsize=\"" str(CPU_XLEN) "\" type=\"code_ptr\"/>"
    "</feature>"
    "</target>";

/* Helper functions for read/write over TCP */
static char *read_packet(char *buffer, size_t size)
{
    int res;
    uint8_t sum;
    char *start, *end;
    size_t i;
    unsigned int checksum;

    res = read(connected_socket, buffer, size);
    if (res <= 0) {
        close(connected_socket);
        connected_socket = -1;
        return NULL;
    }

    sum = 0;
    start = end = NULL;
    for (i = 0; i < size; i++) {
        switch(buffer[i]) {
            case '$':
                start = &buffer[i + 1];
                break;
            case '#':
                end = &buffer[i];
                *end = '\0';
                /* Break from of the outer 'for' loop */
                i = size;
                break;
            default:
                sum += buffer[i];
        }
    }

    /* Perform some sanity checks */
    if (!start || !end || start >= end || res != end - start + 4) {
        dbg_more("GDB server received malformed packet %i bytes", res);
        send(connected_socket, "-", 1, MSG_DONTWAIT | MSG_NOSIGNAL);
        return NULL;
    }

    /* Verify the checksum */
    sscanf(end + 1, "%02x", &checksum);
    if (sum != checksum) {
        dbg_more("GDB server received packet with invalid checksum");
        /* Ask the client to resend the corrupted message */
        send(connected_socket, "-", 1, MSG_DONTWAIT | MSG_NOSIGNAL);
        return NULL;
    }

    /* Let the client know we acknowledge the message */
    send(connected_socket, "+", 1, MSG_DONTWAIT | MSG_NOSIGNAL);

    return start;
}

static void write_packet(char *data)
{
  uint8_t checksum;
  size_t i;
  char end[4], response;

  dbg_more("GDB server sending packet: '%s'", data);

  /* Compute checksum of data to send */
  checksum = 0;
  for (i = 0; i < strlen(data); i++)
    checksum += data[i];
  sprintf(end, "#%02x", checksum);

  /* Send data */
  do {
    send(connected_socket, "$", 1, MSG_DONTWAIT | MSG_NOSIGNAL);
    send(connected_socket, data, strlen(data), MSG_DONTWAIT | MSG_NOSIGNAL);
    send(connected_socket, end, 4, MSG_DONTWAIT | MSG_NOSIGNAL);

    if (read(connected_socket, &response, 1) < 1) {
        die("read failed (GDB client unexpectedly disconnected?)");
    }
  } while (response != '+');
}

/* Each register is 4 or 8 bytes wide and each byte is represented with two
 * ASCII digits. PC is not counted in NREGS. */
#define REG_BUFFER_SIZE ((NREGS + 1) * sizeof(cpu_uint_t) * 2 + 1)

static void handle_reg_read(RISCVCPUState *s)
{
    char buffer[REG_BUFFER_SIZE];

#if CPU_XLEN == 32
#define HTOBE htobe32
#else
#define HTOBE htobe64
#endif

    for (int i = 0; i < NREGS; i++)
        sprintf(buffer + (i * sizeof(cpu_uint_t) * 2), "%" PRIxcpu,
                HTOBE(s->reg[i]));
    sprintf(buffer + (NREGS * sizeof(cpu_uint_t) * 2), "%" PRIxcpu,
            HTOBE(s->pc));
    write_packet(buffer);
}

static const char supported_packet[] = "qSupported";
static const char xfer_features_packet[] = "qXfer:features:read:";

static void handle_query_xfer_features(char *packet)
{
    char *annex;
    size_t offset;
    size_t length;
    int send_size;
    char *buffer;

    annex = strtok(packet + sizeof(xfer_features_packet) - 1, ":");
    offset = strtoul(strtok(NULL, ","), NULL, 16);
    length = strtoul(strtok(NULL, ""), NULL, 16) - 1;

    if (strcmp(annex, "target.xml") || offset > strlen(cpu_desc_xml)) {
        write_packet("E00");
        return;
    } else if (offset == strlen(cpu_desc_xml)) {
        write_packet("l");
        return;
    }

    send_size = min(length, strlen(cpu_desc_xml) - offset);
    buffer = malloc((2 + send_size));
    sprintf(buffer, "m%.*s", send_size, cpu_desc_xml + offset);
    write_packet(buffer);
    free(buffer);
}

static void handle_query(char *packet)
{
    if (!strncmp(packet, supported_packet, strlen(supported_packet)))
        write_packet("qXfer:features:read+;xmlRegisters=riscv" str(CPU_XLEN) ";hwbreak+");
    else if (!strncmp(packet, xfer_features_packet, strlen(xfer_features_packet)))
        handle_query_xfer_features(packet);
    else
        write_packet("");
}

static void handle_mem_read(RISCVCPUState *s, char *packet)
{
    cpu_uint_t addr;
    char *addr_end;
    size_t length;
    uint8_t *data;
    char *buffer;
    int i;

#if CPU_XLEN == 32
#define STRTOUL strtoul
#else
#define STRTOUL strtoull
#endif

    addr = STRTOUL(packet + 1, &addr_end, 16);
    length = strtoul(addr_end + 1, NULL, 16);
    data = malloc(length);
    if (mem_read_debug(s, addr, data, length)) {
        free(data);
        write_packet("E00");
        return;
    }

    buffer = malloc(length * 2 * sizeof(char));
    for (i = 0; i < length; i++)
        sprintf(buffer + (i * 2), "%02x", data[i]);
    write_packet(buffer);

    free(buffer);
    free(data);
}

static void handle_set_breakpoint(char *packet, cpu_uint_t addr)
{
    if (num_breakpoints == MAX_BREAKPOINTS) {
        write_packet("E00");
        return;
    }

    breakpoints[num_breakpoints++] = addr;
    write_packet("OK");
}

static void handle_unset_breakpoint(char *packet, cpu_uint_t addr)
{
    size_t i;

    for (i = 0; i < num_breakpoints; i++)
        if (breakpoints[i] == addr) {
            memmove(&breakpoints[i], &breakpoints[i + 1],
                    (--num_breakpoints - i) * sizeof(cpu_uint_t));
            write_packet("OK");
            return;
        }

    write_packet("E00");
}

static void handle_breakpoints(char *packet)
{
    cpu_uint_t addr;
    char *addr_start;

    if (packet[1] != '0' && packet[1] != '1') {
        write_packet(""); /* Unrecognized breakpoint or watchpoint type */
        return;
    }

    addr_start = strchr(packet, ',');
    if (addr_start == NULL) {
        write_packet("E00"); /* Breakpoint address missing from packet */
        return;
    }

    addr = strtoul(addr_start + 1, NULL, 16);
    if (packet[0] == 'Z')
        handle_set_breakpoint(packet, addr);
    else
        handle_unset_breakpoint(packet, addr);
}

/* Handle incoming packets from GDB, dispatching to respective handlers when
 * appropriate */
static void handle_packet(struct RISCVCPUState *s, char *packet, bool *stop_loop)
{
    dbg_more("GDB server received packet: '%s'", packet);

    switch (packet[0]) {
        case '?':
            write_packet("S05"); /* SIGTRAP */
            break;
        case 'g':
            handle_reg_read(s);
            break;
        case 'm':
            handle_mem_read(s, packet);
            break;
        case 'q':
            handle_query(packet);
            break;
        case 'z':
        case 'Z':
            handle_breakpoints(packet);
            break;
        case 's':
            single_step_flag = true;
        case 'c':
            *stop_loop = true;
            break;
        default:
            write_packet("");
    }
}

/* Repeatedly accept incoming packets from GDB until a continue is requested */
static void command_loop(struct RISCVCPUState *s)
{
    bool stop_loop = false;
    char buffer[PACKET_BUFFER_SIZE];
    char *packet;

    while (!stop_loop) {
        struct pollfd pf;
        pf.fd = connected_socket;
        pf.events = POLLIN | POLLPRI;
        switch (poll(&pf, 1, 100)) {
            case 0:
                continue;
            case 1:
                packet = read_packet(buffer, PACKET_BUFFER_SIZE);
                if (packet)
                    handle_packet(s, packet, &stop_loop);
                break;
            default:
                return;
        }
    }
}

/* Poll for new connections */
static void gdb_poll_connection(struct RISCVCPUState *s)
{
    struct pollfd pf;
    struct sockaddr_in addr;
    char ack;
    int res;

    assert(connected_socket < 0);

    pf.fd = socket_desc;
    pf.events = POLLIN | POLLPRI;
    if (poll(&pf, 1, 0) > 0) {
        socklen_t addr_size = sizeof(addr);
        connected_socket = accept(socket_desc, (struct sockaddr*)&addr,
                &addr_size);

        if (connected_socket < 0)
            return;

        /* Expect a welcome message from GDB */
        res = read(connected_socket, &ack, 1);
        if (res <= 0 || ack != '+') {
            close(connected_socket);
            connected_socket = -1;
            return;
        }

        command_loop(s);
    }
}

/* Public API */
void gdb_init(struct gdb_params *gp)
{
    const int enable = 1;
    struct sockaddr_in addr;

    assert(gp);
    server_enabled = gp->server_enabled;
    if (!server_enabled)
        return;

    socket_desc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket_desc < 0)
      die("socket failed");

    setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));

    memset(&addr, 0, sizeof(addr));
    addr.sin_port = htons(gp->port);
    addr.sin_family = AF_INET;

    if (bind(socket_desc, (struct sockaddr*)&addr, sizeof(addr)))
        die("bind failed (tried to bind port %d)", gp->port);

    if (listen(socket_desc, 1) < 0)
        die("listen failed");

    dbg_info("Waiting for GDB to connect on port %d", gp->port);

    /* Wait for an incoming connection */
    while (connected_socket < 0)
        gdb_poll_connection(riscv_cpu_get_state());
}

void gdb_exit(void)
{
    if (!server_enabled)
        return;

    if (socket_desc >= 0) {
        close(socket_desc);
        socket_desc = -1;
    }

    if (connected_socket >= 0) {
        close(connected_socket);
        connected_socket = -1;
    }

    server_enabled = false;
}

void gdb_poll_interrupt(void)
{
    struct pollfd pf;
    char buffer;

    if (!server_enabled)
        return;

    assert(connected_socket >= 0);
    pf.fd = connected_socket;
    pf.events = POLLIN | POLLPRI;

    /* Poll for interrupt signal from client */
    if (poll(&pf, 1, 0) == 1 && read(connected_socket, &buffer, 1) == 1
            && buffer == INTERRUPT_CODE) {
        write_packet("S05"); /* SIGTRAP */
        command_loop(riscv_cpu_get_state());
    }
}

void gdb_check_breakpoint(cpu_uint_t pc)
{
    size_t i;

    if (!server_enabled)
        return;

    if (unlikely(single_step_flag)) {
        single_step_flag = false;
        write_packet("S05"); /* SIGTRAP */
        command_loop(riscv_cpu_get_state());
    }

    for (i = 0; i < num_breakpoints; i++) {
        if (unlikely(breakpoints[i] == pc)) {
            write_packet("S05"); /* SIGTRAP */
            command_loop(riscv_cpu_get_state());
            return;
        }
    }
}
