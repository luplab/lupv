/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Flattened device tree (FDT) generator
 */
#ifndef FDT_H
#define FDT_H

#include <stdint.h>

/*
 * Opaque FDT type
 */
typedef struct fdt * fdt_t;

/*
 * FDT management
 */
fdt_t fdt_create(void);
void fdt_destroy(fdt_t fdt);

/*
 * Node management
 */
void fdt_begin_node(fdt_t fdt, const char *name);
void fdt_begin_node_num(fdt_t fdt, const char *name, uint32_t n);
void fdt_end_node(fdt_t fdt);

/*
 * Property management
 */
void fdt_prop_blk(fdt_t fdt, const char *prop_name);
void fdt_prop_u32(fdt_t fdt, const char *prop_name, uint32_t val);
void fdt_prop_u32n(fdt_t fdt, const char *prop_name, int n, ...);
void fdt_prop_u64(fdt_t fdt, const char *prop_name, uint64_t val);
void fdt_prop_u64n(fdt_t fdt, const char *prop_name, int n, ...);
void fdt_prop_str(fdt_t fdt, const char *prop_name, const char *str);
void fdt_prop_strn(fdt_t fdt, const char *prop_name, int n, ...);

/*
 * FDT binary generation
 */
int fdt_output(fdt_t fdt, uint8_t *dst);

#endif /* FDT_H */
