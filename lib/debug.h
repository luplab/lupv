/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Debug management
 */
#ifndef DEBUG_H
#define DEBUG_H

#include <lib/utils.h>

/* For each component of the platform, there are four levels of debug */
enum dbg_level {
    DBG_LVL_NONE,   /* Default, no debug unless explicitely required */
    DBG_LVL_CORE,   /* Minimal debug with `-` suffix, such as `-d blk-` */
    DBG_LVL_INFO,   /* Standard debug, such as `-d blk` */
    DBG_LVL_MORE,   /* Extra debug with `+` suffix, such as `-d blk+` */
};

/* Platform components, from a debug point of view */
enum dbg_item {
    DBG_ALL = 0,
    DBG_BLK,
    DBG_CPU_INST, DBG_CPU_FPU, DBG_CPU_MEM, DBG_CPU_SYS,
    DBG_IOMEM,
    DBG_GDB,
    DBG_MACHINE,
    DBG_PIC,
    DBG_RNG,
    DBG_RTC,
    DBG_SYS,
    DBG_TTY,
    DBG_TMR,

    DBG_COUNT,
};

/*
 * Debug management
 */
struct debug_params {
    enum dbg_level items[DBG_COUNT];
    const char *filename;
};

void dbg_init(struct debug_params *dp);
void dbg_exit(void);

void dbg_help(void);
enum dbg_item dbg_lookup(const char *name);

extern enum dbg_level *dbg_items;
#define dbg_item_get_level(item) dbg_items[(item)]


/*
 * Logging functions
 */
__attribute__((format(printf, 3, 4)))
void dbg_log(enum dbg_item item, enum dbg_level level, const char *fmt, ...);

#define dbg(item, lvl, fmt, ...)                         \
    do {                                                 \
        if (unlikely(dbg_item_get_level(item) >= (lvl))) \
            dbg_log(item, lvl, fmt, ## __VA_ARGS__);     \
    } while (0)

#ifdef DBG_ITEM
/* Before platform components include this header, they can define `DBG_ITEM`
 * with their component's name and get access to the following predefined
 * logging macros */
#define dbg_core(fmt, ...) dbg(DBG_ITEM, DBG_LVL_CORE, fmt, ## __VA_ARGS__)
#define dbg_info(fmt, ...) dbg(DBG_ITEM, DBG_LVL_INFO, fmt, ## __VA_ARGS__)
#define dbg_more(fmt, ...) dbg(DBG_ITEM, DBG_LVL_MORE, fmt, ## __VA_ARGS__)
#endif

#endif /* DEBUG_H */
