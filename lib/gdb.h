/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2021 Joël Porquet-Lupine, Noah Rose Ledesma
 *
 * Support for debugging with GDB via its remote serial protocol
 */

#ifndef GDB_H
#define GDB_H

#include <stdbool.h>
#include <stdint.h>

#include <cpu/cpu.h>

struct gdb_params {
    bool server_enabled;
    uint16_t port;
};

void gdb_init(struct gdb_params *gp);
void gdb_exit(void);

/*
 * Check for interrupt messages from a connected GDB client
 */
void gdb_poll_interrupt(void);

/*
 * Check if a breakpoint exists at the specified address,
 * trapping into the remote stub if one does.
 */
void gdb_check_breakpoint(cpu_uint_t pc);

#endif /* GDB_H */
