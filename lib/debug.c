/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Debug management
 */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <lib/debug.h>

enum dbg_level *dbg_items;
static FILE *logfile;

static const struct {
    const char *name;
    const char *help;
} dbg_desc_items[DBG_COUNT] = {
    [DBG_ALL]       = {"all",        "Meta-item enabling all the items below"},
    [DBG_BLK]       = {"blk",        "Block device"},
    [DBG_CPU_INST]  = {"cpu_inst",   "CPU instruction flow"},
    [DBG_CPU_FPU]   = {"cpu_fpu",    "CPU floating-point"},
    [DBG_CPU_MEM]   = {"cpu_mem",    "CPU memory accesses"},
    [DBG_CPU_SYS]   = {"cpu_sys",    "CPU system management"},
    [DBG_IOMEM]     = {"iomem",      "I/O and memory bus accesses"},
    [DBG_GDB]       = {"gdb",        "GDB Server"},
    [DBG_MACHINE]   = {"machine",    "Machine"},
    [DBG_PIC]       = {"pic",        "Programmable interrupt controller"},
    [DBG_RNG]       = {"rng",        "Random number generator device"},
    [DBG_RTC]       = {"rtc",        "Real-time clock device"},
    [DBG_SYS]       = {"sys",        "System controller"},
    [DBG_TTY]       = {"tty",        "Terminal device"},
    [DBG_TMR]       = {"tmr",        "Timer device"},
};

void dbg_init(struct debug_params *dp)
{
    dbg_items = dp->items;

    if (dp->filename)
        logfile = xfopen(dp->filename, "w");
    else
        logfile = stderr;
}

void dbg_exit(void)
{
    fflush(logfile);
    if (logfile != stderr)
        fclose(logfile);
}

void dbg_log(enum dbg_item item, enum dbg_level level, const char *fmt, ...)
{
    va_list ap;

    fprintf(logfile, "%s<%u> ", dbg_desc_items[item].name, level);

    va_start(ap, fmt);
    vfprintf(logfile, fmt, ap);
    va_end(ap);

    fputc('\n', logfile);
}

void dbg_help(void)
{
    int i;

    puts("Debug items\n");
    for (i = 0; i < DBG_COUNT; i++)
        printf("%-15s %s\n", dbg_desc_items[i].name, dbg_desc_items[i].help);
}

enum dbg_item dbg_lookup(const char *name)
{
    enum dbg_item i;

    for (i = 0; i < DBG_COUNT; i++)
        if (!strncmp(name, dbg_desc_items[i].name,
                     strlen(dbg_desc_items[i].name)))
            break;

    return i;
}

