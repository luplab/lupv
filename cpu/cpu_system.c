/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  System management (e.g., exceptions, interrupts, system registers)
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <cpu/cpu_private.h>
#define DBG_ITEM DBG_CPU_SYS
#include <lib/debug.h>
#include <lib/utils.h>

#define CAUSE_INTERRUPT  ((uint64_t)1 << (CPU_XLEN - 1))

#define SSTATUS_MASK (MSTATUS_UIE | MSTATUS_SIE |   \
                      MSTATUS_UPIE | MSTATUS_SPIE | \
                      MSTATUS_SPP |                 \
                      MSTATUS_FS | MSTATUS_XS |     \
                      MSTATUS_SUM | MSTATUS_MXR)

#define MSTATUS_MASK (MSTATUS_UIE | MSTATUS_SIE | MSTATUS_MIE |     \
                      MSTATUS_UPIE | MSTATUS_SPIE | MSTATUS_MPIE |  \
                      MSTATUS_SPP | MSTATUS_MPP |                   \
                      MSTATUS_FS |                                  \
                      MSTATUS_MPRV | MSTATUS_SUM | MSTATUS_MXR)

#define COUNTEREN_CY    (1 << 0)    /* cycle counter */
#define COUNTEREN_IR    (1 << 2)    /* instret counter */

#define COUNTEREN_MASK  (COUNTEREN_CY | COUNTEREN_IR)

/* Return the complete mstatus with the SD bit */
static cpu_uint_t get_mstatus(RISCVCPUState *s, cpu_uint_t mask)
{
    cpu_uint_t val;
    bool sd;

    val = s->mstatus | (s->fs << MSTATUS_FS_SHIFT);
    val &= mask;
    sd = ((val & MSTATUS_FS) == MSTATUS_FS) |
        ((val & MSTATUS_XS) == MSTATUS_XS);
    if (sd)
        val |= (cpu_uint_t)1 << (CPU_XLEN - 1);

    return val;
}

static void set_mstatus(RISCVCPUState *s, cpu_uint_t val)
{
    cpu_uint_t mod, mask;

    /* Flush the TLBs if change of MMU config */
    mod = s->mstatus ^ val;
    if ((mod & (MSTATUS_MPRV | MSTATUS_SUM | MSTATUS_MXR)) != 0 ||
        ((s->mstatus & MSTATUS_MPRV) && (mod & MSTATUS_MPP) != 0)) {
        tlb_flush_all(s);
    }
    s->fs = (val >> MSTATUS_FS_SHIFT) & 3;

    mask = MSTATUS_MASK & ~MSTATUS_FS;
    s->mstatus = (s->mstatus & ~mask) | (val & mask);
}

int csr_read(RISCVCPUState *s, cpu_uint_t *pval, uint32_t csr, bool will_write)
{
    cpu_uint_t val;

    /* Field `will_write` indicates that the CSR will be written after (used for
     * CSR access check) */
    if (((csr & 0xc00) == 0xc00) && will_write)
        return -1; /* read-only CSR */
    if (s->priv < ((csr >> 8) & 3))
        return -1; /* not enough priviledge */

    switch(csr) {
        /* User floating-point CSRs */
        case 0x001: /* fflags */
            if (s->fs == 0)
                return -1;
            val = s->fflags;
            break;
        case 0x002: /* frm */
            if (s->fs == 0)
                return -1;
            val = s->frm;
            break;
        case 0x003: /* fcsr */
            if (s->fs == 0)
                return -1;
            val = s->fflags | (s->frm << 5);
            break;

        /* User counters/timers */
        case 0xc00: /* cycle */
        case 0xc02: /* instret */
            /* Both are equivalent because we assume an IPC of 1 */
            {
                uint32_t counteren;
                if (s->priv < PRV_M) {
                    if (s->priv < PRV_S)
                        counteren = s->scounteren;
                    else
                        counteren = s->mcounteren;
                    if (((counteren >> (csr & 0x1f)) & 1) == 0)
                        goto invalid_csr;
                }
            }
            val = (int64_t)s->insn_counter;
            break;
        case 0xc80: /* cycleh */
        case 0xc82: /* instreth */
            {
                uint32_t counteren;
                if (s->priv < PRV_M) {
                    if (s->priv < PRV_S)
                        counteren = s->scounteren;
                    else
                        counteren = s->mcounteren;
                    if (((counteren >> (csr & 0x1f)) & 1) == 0)
                        goto invalid_csr;
                }
            }
            val = s->insn_counter >> 32;
            break;

        /* Supervisor trap setup */
        case 0x100: /* sstatus */
            val = get_mstatus(s, SSTATUS_MASK);
            break;
        case 0x104: /* sie */
            val = s->mie & s->mideleg;
            break;
        case 0x105: /* stvec */
            val = s->stvec;
            break;
        case 0x106: /* scounteren */
            val = s->scounteren;
            break;

        /* Supervisor trap handling */
        case 0x140: /*sscratch */
            val = s->sscratch;
            break;
        case 0x141: /* sepc */
            val = s->sepc;
            break;
        case 0x142: /* scause */
            val = s->scause;
            break;
        case 0x143: /* stval */
            val = s->stval;
            break;
        case 0x144: /* sip */
            val = s->mip & s->mideleg;
            break;

        /* Supervisor protection and translation */
        case 0x180: /* satp */
            val = s->satp;
            break;

        /* Machine trap setup */
        case 0x300: /* mstatus */
            val = get_mstatus(s, (cpu_uint_t)-1);
            break;
        case 0x301: /* misa */
            val = s->misa;
            break;
        case 0x302: /* medeleg */
            val = s->medeleg;
            break;
        case 0x303: /* mideleg */
            val = s->mideleg;
            break;
        case 0x304: /* mie */
            val = s->mie;
            break;
        case 0x305: /* mtvec */
            val = s->mtvec;
            break;
        case 0x306: /* mcounteren */
            val = s->mcounteren;
            break;

        /* Machine trap handling */
        case 0x340: /* mscratch */
            val = s->mscratch;
            break;
        case 0x341: /* mepc */
            val = s->mepc;
            break;
        case 0x342: /* mcause */
            val = s->mcause;
            break;
        case 0x343: /* mtval */
            val = s->mtval;
            break;
        case 0x344: /* mip */
            val = s->mip;
            break;

        /* Machine counters/timers */
        case 0xb00: /* mcycle */
        case 0xb02: /* minstret */
            val = (int64_t)s->insn_counter;
            break;
        case 0xb80: /* mcycleh */
        case 0xb82: /* minstreth */
#if CPU_XLEN == 32
            val = s->insn_counter >> 32;
#else
            goto invalid_csr;
#endif
            break;

        /* Machine information registers */
        case 0xf14:
            val = s->mhartid;
            break;

        default:
invalid_csr:
            /* The 'time' counter is usually emulated */
            if (csr != 0xc01 && csr != 0xc81)
                dbg_core("%s: invalid csr=0x%03x", __func__, csr);

            *pval = 0;
            return -1;
    }

    dbg_more("%s: csr=0x%03x val=0x%" PRIxcpu, __func__, csr, val);
    *pval = val;
    return 0;
}

static void set_frm(RISCVCPUState *s, unsigned int val)
{
    if (val >= 5)
        val = 0;
    s->frm = val;
}

/* return -1 if invalid CSR, 0 if OK */
int csr_write(RISCVCPUState *s, uint32_t csr, cpu_uint_t val)
{
    cpu_uint_t mask;

    dbg_more("%s: csr=0x%03x val=0x%" PRIxcpu, __func__, csr, val);

    switch(csr) {
        /* User floating-point CSRs */
        case 0x001: /* fflags */
            s->fflags = val & 0x1f;
            s->fs = 3;
            break;
        case 0x002: /* frm */
            set_frm(s, val & 7);
            s->fs = 3;
            break;
        case 0x003: /* fcsr */
            set_frm(s, (val >> 5) & 7);
            s->fflags = val & 0x1f;
            s->fs = 3;
            break;

        /* Supervisor trap setup */
        case 0x100: /* sstatus */
            set_mstatus(s, (s->mstatus & ~SSTATUS_MASK) | (val & SSTATUS_MASK));
            break;
        case 0x104: /* sie */
            mask = s->mideleg;
            s->mie = (s->mie & ~mask) | (val & mask);
            break;
        case 0x105: /* stvec */
            s->stvec = val & ~3;
            break;
        case 0x106:
            s->scounteren = val & COUNTEREN_MASK;
            break;

        /* Supervisor trap handling */
        case 0x140: /* sscratch */
            s->sscratch = val;
            break;
        case 0x141: /* sepc */
            s->sepc = val & ~1;
            break;
        case 0x142: /* scause */
            s->scause = val;
            break;
        case 0x143: /* stval */
            s->stval = val;
            break;
        case 0x144: /* sip */
            mask = s->mideleg;
            s->mip = (s->mip & ~mask) | (val & mask);
            break;

        /* Supervisor protection and translation */
        case 0x180: /* satp */
            /* ASID not implemented (since no real cache or TLB using ASID) */
#if CPU_XLEN == 32
            s->satp = (val & ((cpu_uint_t)1 << 31))     /* mode */
                | (val & (((cpu_uint_t)1 << 22) - 1));  /* ppn */
#else
            {
                cpu_uint_t mode, new_mode;
                mode = s->satp >> 60;   /* Save current mode */
                new_mode = val >> 60;
                /* Supports only Bare, Sv39, and Sv48 */
                if (new_mode == 0 || (new_mode >= 8 && new_mode <= 9))
                    mode = new_mode;
                s->satp = ((cpu_uint_t)mode << 60) |      /* mode */
                    (val & (((cpu_uint_t)1 << 44) - 1));  /* ppn */
            }
#endif
            tlb_flush_all(s);
            break;

        /* Machine trap setup */
        case 0x300: /* mstatus */
            set_mstatus(s, val);
            break;
        case 0x301: /* misa */
            /* Changing MXL field dynamically is not supported */
            break;
        case 0x302: /* medeleg */
            mask = (1 << (CAUSE_STORE_PAGE_FAULT + 1)) - 1;
            s->medeleg = (s->medeleg & ~mask) | (val & mask);
            break;
        case 0x303: /* mideleg */
            mask = MIP_SSIP | MIP_STIP | MIP_SEIP;
            s->mideleg = (s->mideleg & ~mask) | (val & mask);
            break;
        case 0x304: /* mie */
            mask = MIP_MSIP | MIP_MTIP | MIP_SSIP | MIP_STIP | MIP_SEIP;
            s->mie = (s->mie & ~mask) | (val & mask);
            break;
        case 0x305: /* mtvec */
            s->mtvec = val & ~3;
            break;
        case 0x306: /* mcounteren */
            s->mcounteren = val & COUNTEREN_MASK;
            break;
        case 0x340: /* mscratch */
            s->mscratch = val;
            break;
        case 0x341: /* mepc */
            s->mepc = val & ~1;
            break;
        case 0x342: /* mcause */
            s->mcause = val;
            break;
        case 0x343: /* mtval */
            s->mtval = val;
            break;
        case 0x344: /* mip */
            mask = MIP_SSIP | MIP_STIP;
            s->mip = (s->mip & ~mask) | (val & mask);
            break;

        default:
            dbg_core("%s: invalid csr=0x%03x", __func__, csr);
            return -1;
    }
    return 0;
}

static void set_priv(RISCVCPUState *s, int priv)
{
    if (s->priv != priv) {
        tlb_flush_all(s);
        s->priv = priv;
    }
}

void raise_exception2(RISCVCPUState *s, cpu_uint_t cause, cpu_uint_t tval)
{
    bool deleg;

    dbg_info("%s: cause=0x%" PRIxcpu " tval=0x%" PRIxcpu,
             __func__, cause, tval);

    if (s->priv <= PRV_S) {
        /* Delegate the exception to the supervisor priviledge */
        if (cause & CAUSE_INTERRUPT)
            deleg = (s->mideleg >> (cause & (CPU_XLEN - 1))) & 1;
        else
            deleg = (s->medeleg >> cause) & 1;
    } else {
        deleg = 0;
    }

    if (deleg) {
        s->scause = cause;
        s->sepc = s->pc;
        s->stval = tval;
        s->mstatus = (s->mstatus & ~MSTATUS_SPIE) |
            (((s->mstatus >> s->priv) & 1) << MSTATUS_SPIE_SHIFT);
        s->mstatus = (s->mstatus & ~MSTATUS_SPP) |
            (s->priv << MSTATUS_SPP_SHIFT);
        s->mstatus &= ~MSTATUS_SIE;
        set_priv(s, PRV_S);
        s->pc = s->stvec;
    } else {
        s->mcause = cause;
        s->mepc = s->pc;
        s->mtval = tval;
        s->mstatus = (s->mstatus & ~MSTATUS_MPIE) |
            (((s->mstatus >> s->priv) & 1) << MSTATUS_MPIE_SHIFT);
        s->mstatus = (s->mstatus & ~MSTATUS_MPP) |
            (s->priv << MSTATUS_MPP_SHIFT);
        s->mstatus &= ~MSTATUS_MIE;
        set_priv(s, PRV_M);
        s->pc = s->mtvec;
    }
}

void raise_exception(RISCVCPUState *s, cpu_uint_t cause)
{
    raise_exception2(s, cause, 0);
}

void handle_sret(RISCVCPUState *s)
{
    int spp, spie;

    spp = (s->mstatus >> MSTATUS_SPP_SHIFT) & 1;
    /* Set the IE state to previous IE state */
    spie = (s->mstatus >> MSTATUS_SPIE_SHIFT) & 1;
    s->mstatus = (s->mstatus & ~(1 << spp)) |
        (spie << spp);
    /* Set SPIE to 1 */
    s->mstatus |= MSTATUS_SPIE;
    /* Set SPP to U */
    s->mstatus &= ~MSTATUS_SPP;
    set_priv(s, spp);
    s->pc = s->sepc;
}

void handle_mret(RISCVCPUState *s)
{
    int mpp, mpie;

    mpp = (s->mstatus >> MSTATUS_MPP_SHIFT) & 3;
    /* Set the IE state to previous IE state */
    mpie = (s->mstatus >> MSTATUS_MPIE_SHIFT) & 1;
    s->mstatus = (s->mstatus & ~(1 << mpp)) |
        (mpie << mpp);
    /* Set MPIE to 1 */
    s->mstatus |= MSTATUS_MPIE;
    /* Set MPP to U */
    s->mstatus &= ~MSTATUS_MPP;
    set_priv(s, mpp);
    s->pc = s->mepc;
}

static inline uint32_t get_pending_irq_mask(RISCVCPUState *s)
{
    uint32_t pending_ints, enabled_ints;

    pending_ints = s->mip & s->mie;
    if (pending_ints == 0)
        return 0;

    enabled_ints = 0;
    switch(s->priv) {
        case PRV_M:
            if (s->mstatus & MSTATUS_MIE)
                enabled_ints = ~s->mideleg;
            break;
        case PRV_S:
            enabled_ints = ~s->mideleg;
            if (s->mstatus & MSTATUS_SIE)
                enabled_ints |= s->mideleg;
            break;
        default:
        case PRV_U:
            enabled_ints = -1;
            break;
    }

    return pending_ints & enabled_ints;
}

void raise_interrupt(RISCVCPUState *s)
{
    uint32_t mask;
    int irq_num;

    mask = get_pending_irq_mask(s);
    if (mask == 0)
        return;
    irq_num = ctz32(mask);
    raise_exception(s, irq_num | CAUSE_INTERRUPT);
}
