/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Public functions
 */

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <cpu/cpu_private.h>
#include <lib/utils.h>
#include <platform/iomem.h>

static RISCVCPUState cpu_state;

RISCVCPUState *riscv_cpu_get_state(void)
{
    return &cpu_state;
}

void riscv_cpu_interp(int n_cycles)
{
    RISCVCPUState *s;
    uint64_t timeout;

    s = &cpu_state;

    timeout = s->insn_counter + n_cycles;
    while (!s->is_stalled && (int)(timeout - s->insn_counter) > 0) {
        n_cycles = timeout - s->insn_counter;
        riscv_cpu_interp_rv(s, n_cycles);
    }
}

static inline void riscv_cpu_set_mip(uint32_t mask, bool state)
{
    if (state) {
        /* Raise IRQ */
        cpu_state.mip |= mask;

        /* Resume CPU if an IRQ is pending */
        if (cpu_state.is_stalled && (cpu_state.mip & cpu_state.mie) != 0)
            cpu_state.is_stalled = false;
    } else {
        /* Clear IRQ */
        cpu_state.mip &= ~mask;
    }
}

void riscv_cpu_set_irq(int irq, bool state)
{
    assert (irq >= IRQ_U_SOFT && irq <= IRQ_M_EXT);
    riscv_cpu_set_mip(1UL << irq, state);
}

bool riscv_cpu_has_timer_irq(void)
{
    return !!(cpu_state.mip & (MIP_MTIP | MIP_HTIP | MIP_STIP | MIP_UTIP));
}

bool riscv_cpu_is_stalled(void)
{
    return cpu_state.is_stalled;
}

void riscv_cpu_init(cpu_uint_t boot_addr)
{
    RISCVCPUState *s;

    s = &cpu_state;
    s->pc = boot_addr;
    s->priv = PRV_M;
    s->misa = (cpu_uint_t)MISA_MXL << (CPU_XLEN - 2);
    s->misa |= MISA_SUPER | MISA_USER;
    s->misa |= MISA_I | MISA_M | MISA_A | MISA_F | MISA_D; /* == 'G' */
    s->misa |= MISA_C;
#if CPU_XLEN == 64
    s->mstatus = (cpu_uint_t)MISA_MXL << MSTATUS_UXL_SHIFT |
        (cpu_uint_t)MISA_MXL << MSTATUS_SXL_SHIFT;
#endif
    tlb_init(s);
}

