/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 * Copyright (c) 2020 Joël Porquet-Lupine
 *
 * Memory and IO/device management
 */

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define DBG_ITEM DBG_IOMEM
#include <lib/debug.h>
#include <lib/utils.h>
#include <platform/iomem.h>

struct iomem_region {
    phys_addr_t addr;
    phys_addr_t size;
    bool is_ram;
    union {
        /* RAM support */
        uint8_t *phys_mem;

        /* I/O device support */
        struct {
            void *dev;
            struct iomem_dev_ops *ops;
        };
    };
};

#define IOMEM_MAX_REGIONS 16

struct iomem_map {
    int count;
    struct iomem_region regions[IOMEM_MAX_REGIONS];
};

static struct iomem_map iomem_map;

void iomem_register_ram(phys_addr_t addr, phys_addr_t size)
{
    struct iomem_region *r;

    assert(iomem_map.count < IOMEM_MAX_REGIONS);

    r = &iomem_map.regions[iomem_map.count++];
    r->addr = addr;
    r->size = size;
    r->is_ram = true;
    r->phys_mem = xmallocz(size);
}

void iomem_register_device(phys_addr_t addr, phys_addr_t size,
                           void *dev, struct iomem_dev_ops *ops)
{
    struct iomem_region *r;

    assert(iomem_map.count < IOMEM_MAX_REGIONS);

    r = &iomem_map.regions[iomem_map.count++];
    r->addr = addr;
    r->size = size;
    r->is_ram = false;
    r->dev = dev;
    r->ops = ops;
}

iomem_t iomem_get_region(phys_addr_t addr)
{
    struct iomem_region *r;
    int i;

    for(i = 0; i < iomem_map.count; i++) {
        r = &iomem_map.regions[i];
        if (addr >= r->addr && addr < r->addr + r->size)
            return r;
    }

    return NULL;
}

bool iomem_region_is_ram(iomem_t reg)
{
    assert(reg);

    return reg->is_ram;
}

phys_addr_t iomem_region_addr(iomem_t reg)
{
    assert(reg);

    return reg->addr;
}

phys_addr_t iomem_region_size(iomem_t reg)
{
    assert(reg);

    return reg->size;
}

uint8_t *iomem_ram_get_ptr(iomem_t reg, phys_addr_t addr)
{
    assert(reg && reg->is_ram
           && addr >= reg->addr && addr < reg->addr + reg->size);

    return reg->phys_mem + (uintptr_t)(addr - reg->addr);
}

uint64_t iomem_ram_read(iomem_t reg, phys_addr_t addr,
                        enum iomem_size_log2 size_log2)
{
    uint64_t val;
    uint8_t *ptr;

    ptr = iomem_ram_get_ptr(reg, addr);
    switch(size_log2) {
        case IOMEM_1BYTE_SIZE_LOG2:
            val = *(uint8_t *)ptr;
            break;
        case IOMEM_2BYTE_SIZE_LOG2:
            val = *(uint16_t *)ptr;
            break;
        case IOMEM_4BYTE_SIZE_LOG2:
            val = *(uint32_t *)ptr;
            break;
        case IOMEM_8BYTE_SIZE_LOG2:
            val = *(uint64_t *)ptr;
            break;
        default:
            die("bug: unhandled size_log2 %d", size_log2);
    }

    return val;
}

void iomem_ram_write(iomem_t reg, phys_addr_t addr, uint64_t val,
                     enum iomem_size_log2 size_log2)
{
    uint8_t *ptr;

    ptr = iomem_ram_get_ptr(reg, addr);
    switch(size_log2) {
        case IOMEM_1BYTE_SIZE_LOG2:
            *(uint8_t *)ptr = val;
            break;
        case IOMEM_2BYTE_SIZE_LOG2:
            *(uint16_t *)ptr = val;
            break;
        case IOMEM_4BYTE_SIZE_LOG2:
            *(uint32_t *)ptr = val;
            break;
        case IOMEM_8BYTE_SIZE_LOG2:
            *(uint64_t *)ptr = val;
            break;
        default:
            die("bug: unhandled size_log2 %d", size_log2);
    }
}

uint64_t iomem_dev_read(iomem_t reg, phys_addr_t addr,
                        enum iomem_size_log2 size_log2)
{
    phys_addr_t offset;

    assert(reg && !reg->is_ram
           && addr >= reg->addr && addr < reg->addr + reg->size);

    if (((reg->ops->flags >> size_log2) & 1) == 0) {
        int nbytes = 1 << size_log2;
        dbg_core("%s: unsupported read access at addr=0x%" PRIxpa
                 " size=%d bytes",
                 __func__,  addr, nbytes);
        return 0;
    }

    offset = addr - reg->addr;

    return reg->ops->read(reg->dev, offset, size_log2);
}

void iomem_dev_write(iomem_t reg, phys_addr_t addr, uint64_t val,
                     enum iomem_size_log2 size_log2)
{
    phys_addr_t offset;

    assert(reg && !reg->is_ram
           && addr >= reg->addr && addr < reg->addr + reg->size);

    if (((reg->ops->flags >> size_log2) & 1) == 0) {
        int nbytes = 1 << size_log2;
        dbg_core("%s: unsupported write access at addr=0x%" PRIxpa
                 " size=%d bytes",
                 __func__,  addr, nbytes);
        return;
    }

    offset = addr - reg->addr;
    reg->ops->write(reg->dev, offset, val, size_log2);
}

void iomem_exit(void)
{
    int i;
    struct iomem_region *r;

    for(i = 0; i < iomem_map.count; i++) {
        r = &iomem_map.regions[i];
        if (r->is_ram)
            free(r->phys_mem);
    }
}

