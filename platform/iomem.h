/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 * Copyright (c) 2020 Joël Porquet-Lupine
 *
 * Memory and I/O device management
 */

#ifndef IOMEM_H
#define IOMEM_H

#include <stdbool.h>
#include <stdint.h>

#include <cpu/cpu.h>

/*
 * A memory region represents a portion of the physical address space and gives
 * access to a memory bank (RAM/ROM) or an I/O device.
 */
struct iomem_region;
typedef struct iomem_region * iomem_t;

iomem_t iomem_get_region(phys_addr_t addr);

phys_addr_t iomem_region_addr(iomem_t reg);
phys_addr_t iomem_region_size(iomem_t reg);

bool iomem_region_is_ram(iomem_t reg);
uint8_t *iomem_ram_get_ptr(iomem_t reg, phys_addr_t addr);

/*
 * Memory accesses
 */
enum iomem_size_log2 {
    IOMEM_1BYTE_SIZE_LOG2 = 0,
    IOMEM_2BYTE_SIZE_LOG2 = 1,
    IOMEM_4BYTE_SIZE_LOG2 = 2,
    IOMEM_8BYTE_SIZE_LOG2 = 3
};

/* Memory banks */
void iomem_register_ram(phys_addr_t addr, phys_addr_t size);

uint64_t iomem_ram_read(iomem_t reg, phys_addr_t addr,
                        enum iomem_size_log2 size_log2);
void iomem_ram_write(iomem_t reg, phys_addr_t addr, uint64_t val,
                     enum iomem_size_log2 size_log2);

/* I/O devices */
#define IOMEM_DEV_SIZE8  (1 << IOMEM_1BYTE_SIZE_LOG2)
#define IOMEM_DEV_SIZE16 (1 << IOMEM_2BYTE_SIZE_LOG2)
#define IOMEM_DEV_SIZE32 (1 << IOMEM_4BYTE_SIZE_LOG2)
#define IOMEM_DEV_SIZE64 (1 << IOMEM_8BYTE_SIZE_LOG2)

struct iomem_dev_ops {
    void (*write)(void *dev, phys_addr_t offset, uint64_t val,
                  enum iomem_size_log2 size_log2);
    uint64_t (*read)(void *dev, phys_addr_t offset,
                     enum iomem_size_log2 size_log2);
    int flags;
};

void iomem_register_device(phys_addr_t addr, phys_addr_t size,
                           void *dev, struct iomem_dev_ops *ops);

uint64_t iomem_dev_read(iomem_t reg, phys_addr_t addr,
                        enum iomem_size_log2 size_log2);
void iomem_dev_write(iomem_t reg, phys_addr_t addr, uint64_t val,
                     enum iomem_size_log2 size_log2);

/*
 * Cleanup
 */
void iomem_exit(void);

#endif /* IOMEM_H */
