# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020 Joël Porquet-Lupine

# General compiler options
## Ensure warnings are caught
CFLAGS = -Wall
## C99 with GNU extensions
CFLAGS += -std=gnu99
## Avoid temporary files during build
CFLAGS += -pipe
## Generate dependency files
CFLAGS += -MMD -MP
## Force 64-bit file system interface
CFLAGS += -D'_FILE_OFFSET_BITS=64'
ifneq ($(D),1)
## Optimization level
CFLAGS += -O2
else
## Debug flags --and no optimizations
CFLAGS += -g
endif
CFLAGS += -I.

# Version
## Get software version from git repo
VERSION := $(shell git describe --dirty 2>/dev/null)
ifneq ($(VERSION),)
CFLAGS += -D'VERSION="$(VERSION)"'
endif

# Resource directory (e.g., BIOS)
ifeq ($(R),)
res := $(CURDIR)
else
res := $(shell readlink -f $(R))
endif
CFLAGS += -D'RESDIR="$(res)"'

# Build directory
ifeq ($(O),)
build := $(CURDIR)/build
else
build := $(shell readlink -f $(O))
endif

# CPU width (32 bits by default)
ifeq ($(W),64)
width := 64
else
width := 32
endif
CFLAGS += -DCPU_XLEN=$(width)

# Files
## C files
src := $(sort $(shell find . -name '*.c' -type f 2>/dev/null))
## Object files
obj := $(addprefix $(build)/, $(patsubst %.c,%.o,$(src)))
## Dependency files
dep := $(patsubst %.o,%.d,$(obj))

# Verbosity
ifneq ($(V),1)
Q := @
else
Q :=
endif

# Main rule
all: _build lupv$(width)

# Create build directories
_build: $(dir $(obj))

$(sort $(dir $(obj))):
	@mkdir -p $@

# Build program
lupv$(width): $(build)/lupv
	@cp $< $@

# Link rule
$(build)/lupv: $(obj)
	@echo "LD	$(notdir $@)"
	$(Q)$(CC) $(CFLAGS) -o $@ $^

# Compilation rules
$(build)/%.o: %.c
	@echo "CC	$(notdir $@)"
	$(Q)$(CC) $(CFLAGS) -c -o $@ $<

# Maintenance rule
clean:
	@echo "CLEAN"
	$(Q)$(RM) -r $(build) lupv$(width)

# Help rule
help:
	@echo "  make W=32|64   CPU width (default is 32)"
	@echo "  make V=1       Verbose build"
	@echo "  make O=<dir>   Build directory"
	@echo "  make D=1       Enable debug flag and no optimization"
	@echo "  make R=<dir>   Path to resource directory (e.g. default BIOS image)"

# Rules configuration
.PHONY: _build clean

# Include source code dependencies
-include $(dep)
